<?php

namespace App\Http\Controllers;

use App\Http\Requests\TestRequestCreate;
use Illuminate\Http\Request;

class TestController extends Controller
{

    public function index()
    {
        return view('test');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $result = $this->specialValidation($request->all());
        $data = new \stdClass();

        $data->messages = $result?"<p class='error text-center'>Something went wrong</p>":"<p class='success text-center'>Test field was be created successfully</p>";
        $data->error = $result;
        $data->attributes = $request->all();

        return response()->json($data);
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    protected function specialValidation($attributes){
        $test = $attributes['inputWord'];
        $error = [];
        if(strlen($test) < 1) $error[] = 'The test field is required. <br>';
        if(strlen($test) < $attributes['minValue']  && $attributes['minValue'] != 0) $error[] = 'The test must be at least '.$attributes['minValue'].' characters <br/>';
        if(strlen($test) > $attributes['maxValue'] && $attributes['maxValue'] != 0) $error[] = 'The test may not be greater than '.$attributes['maxValue'].' characters <br/>';


        $rules = array(
            3=>$attributes['third'],
            4=>$attributes['fourth'],
            5=>$attributes['five'],
        );

        foreach ($rules as $index=>$value){
            if($value != 'letter' && $value != 'number'){
                if(strlen($value) > 1) continue;
            }

            if(strlen($value) > 0) {
                switch ($value){
                    case 'letter':
                        if(strtoupper(substr($test,$index-1,1)) === strtolower(substr($test,$index-1,1))){
                            $error[] = $index.' character should be a Letter. <br/>';
                        };
                        break;
                    case 'number':
                        if(!is_numeric(substr($test,$index-1,1))) {
                            $error[] = $index.' character should be a Number. <br/>';
                        }
                        break;
                    default:
                        if(substr($test,$index-1,1) !== $value) {
                            $error[] = $index.' character should be <i> '.$value.' </i><br/>';
                        }
                }
            }
        }

        return $error;
    }
}
