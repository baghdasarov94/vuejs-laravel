<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('test');
})->name('home');

Route::group(['prefix' => 'test'], function () {
    Route::get('/', [
        'as' => 'test',
        'uses' => 'TestController@index'
    ]);

    Route::post('/', [
        'as' => 'test-store',
        'uses' => 'TestController@store'
    ]);

});

