new Vue({
    el:"#app",
    data () {
        return {
            formData:{
                inputWord: '',
                minValue: '',
                maxValue: '',
                third: '',
                fourth: '',
                five: '',
                _token:TokenCsrf.csrfToken,
            },
            results:[],
            arrayOfValue: [],
            error: ''
        }
    },
    methods: {
        notNegotive(){
            let self = this;

            if(self.formData.minValue < 0){
                self.formData.minValue = 0;
            };
            if(self.formData.maxValue < 0){
                self.formData.maxValue = 0;
            };

            if(self.formData.maxValue != 0){
                if(self.formData.maxValue < self.formData.minValue){
                    self.formData.minValue = self.formData.maxValue;
                };
            };
            self.stringToArray();
        },

        stringToArray () {
            let self = this;
            let msg = '';
            self.arrayOfValue = self.formData.inputWord.split('');
            if (this.arrayOfValue.length < self.formData.minValue && self.formData.minValue != 0) {
                msg += 'The test must be at least '+self.formData.minValue+' characters.<br/>'
            }
            if (this.arrayOfValue.length > self.formData.maxValue && self.formData.maxValue !=0) {
                msg += 'The test may not be greater than '+self.formData.maxValue+' characters.<br/>'
            }

            let rulesObj = {
                3:self.formData.third,
                4:self.formData.fourth,
                5:self.formData.five
            };

            $.each(rulesObj,function (index,data) {
                if(data != 'letter' && data != 'number'){
                    if(data.length > 1) data = '';
                }
                if(data.length > 0) {
                    switch (data){
                        case 'letter':
                            if (typeof self.arrayOfValue[index-1] === 'string' && self.arrayOfValue[index-1].toLowerCase() === self.arrayOfValue[index-1].toUpperCase()) {
                                msg += index+' character should be a Letter.<br/>'
                            }
                            break;
                        case 'number':
                            if (typeof self.arrayOfValue[index-1] === 'string' && isNaN(parseInt(self.arrayOfValue[index-1]))) {
                                msg += index+' character should be Number.<br/> '
                            }
                            break;
                        default:
                            if (typeof self.arrayOfValue[index-1] === 'string' && self.arrayOfValue[index-1] !== data) {
                                msg += index+' character should be '+data+'.<br/>'
                            }
                    }
                }
            });
            self.error = msg
        },

        save(){
            let self = this;
            let formData = self.formData;
            this.$http.post('test',formData)
                .then(response => {
                    self.results = response.body;
                }, response => {
                    alert('Unidentified error');
                });

        }

    }
})