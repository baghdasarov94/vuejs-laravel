<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="robots" content="index, follow">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta property="og:url" content="">
        <meta property="og:type" content="website">
        <meta property="og:title" content="">
        <meta property="og:description" content="">
        <meta property="og:image" content="">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet' type='text/css'>

        <title>Test Vue/Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
            window.TokenCsrf = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]); !!}
        </script>
    </head>
    <body>
        <button class="btn btn-success removeAnimation">Remove Animation</button>
        <canvas></canvas>
        <div id="app">
            <div class="col-md-6 m-t-150 col-md-offset-3 mainContent">
                <div class='col-lg-12'>
                    <h2 class="headerTitle">Welcome</h2>
                    <div v-html="results.messages"></div>
                        <div class="col-md-6 testContent">
                            <div class="testRulesContent">
                                <h2 class="col-md-12">Rules</h2>

                                <input class="col-md-12" name='min' min="0" v-model="formData.minValue" value="" placeholder='Min characters' @keyup="notNegotive" type='number'>
                                <input class="col-md-12" name='max' min="0" v-model="formData.maxValue" value="" placeholder='Max characters'  @keyup="notNegotive" type='number'>
                                <input class="col-md-12" name='third' @keyup="stringToArray" v-model="formData.third" value="" placeholder='Third characters' type='text'>
                                <input class="col-md-12" name='fourth' @keyup="stringToArray" v-model="formData.fourth" value="" placeholder='Fourth characters' type='text'>
                                <input class="col-md-12" name='five' @keyup="stringToArray" v-model="formData.five" value="" placeholder='Five character' type='text'>
                            </div>
                        </div>
                        <div class="col-md-6 m-t-100 testContent">
                            <div class="testInputContent">
                                <input v-model="formData.inputWord" @keyup="stringToArray" class="col-md-8" name='test' value="" placeholder='Please type' type='text' onchange="removeTmpText()">
                                <input class='animated col-md-4' type='button' @click="save" value='GO'>
                                <div class="col-md-12 col-md-offset-0 m-t-20">
                                    <p class="error text-center" v-html="error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3 m-t-20">
                            <div class="error text-center" v-for="error in results.error">
                                <span v-html="error"></span>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/convas.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.removeAnimation').click(function () {
                $("canvas").toggle();
                $('.removeAnimation').text('Show Animation');
            })
            $(".isDisabble").prop('disabled', true);
        });
        
        function removeTmpText() {
            $(".success").text('');
            $(".error").text('');
        }
    </script>
</html>
